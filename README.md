# Proyecto 3 Computacion Grafica

## Reconstruccion de imagenes

### Algoritmo

Para poder realizar esta reconstruccion se siguio el siguiente algoritmo

1. Se lee el directorio de imagenes y se cargan una por una
2. Para cada imagen se filtran los puntos en base al color, gracias a un valor minimo podemos verificar si el color esta o no dentro de el rango
3. Se crean nuevas imagenes que mapean colores planos, negro para los elementos ignorados y blancos para los puntos guardados
4. Despues de todo eso se extraen los puntos en base a lo siguiente:
   1. Recorremos todas las imagenes planas guardadas y se leen cada uno de sus XY con lo que se guardan esos valores, ademas se usa un contador para definir un valor Z, cabe destacar que unicamente se filtran los puntos negros mientras que los puntos blancos se aceptan
5. Finalmente el programa se encarga de renderizar todos los puntos