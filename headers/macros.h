#pragma once

#define cimg_use_jpeg
#define GL_SILENCE_DEPRECATION
#define ANG2RAD (3.14159265358979323846 / 180.0)

#include <GL/glew.h>

#include <GL/freeglut.h>

#include "./CImg.h"
#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <vector>

namespace fs = std::filesystem;
using namespace std;
using namespace cimg_library;
