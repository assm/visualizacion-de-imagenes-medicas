#include "./headers/globals.h"
#include "./headers/macros.h"
#include "./structures/screen.h"

void resize(int w, int h) { screen->resize(w, h); }
void draw() { screen->draw(); }
void motionFunc(int x, int y) { screen->motionFunc(x, y); }

auto main(int argc, char **argv) -> int {
  screen = new screen_t();
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  int posX = (glutGet(GLUT_SCREEN_WIDTH) - 500) >> 1;
  int posY = (glutGet(GLUT_SCREEN_HEIGHT) - 500) >> 1;
  glutInitWindowPosition(posX, posY);
  glutInitWindowSize(500, 500);
  glutCreateWindow("Visualizacion de Cerebro");
  glutReshapeFunc(resize);
  glutDisplayFunc(draw);
  glutMotionFunc(motionFunc);
  glEnable(GL_DEPTH_TEST);
  glPointSize(1.0);
  glutMainLoop();
  delete screen;
  return 0;
}
