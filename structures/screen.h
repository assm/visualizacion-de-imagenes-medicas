#pragma once
#include "../headers/macros.h"
#include "./model.h"

class screen_t {
private:
  model m;
  GLfloat Xmouse;
  GLfloat Ymouse;

public:
  static GLfloat rotationMatrix[16];

public:
  screen_t() : Xmouse(0), Ymouse(0) {
    vector<string> paths;
    for (const auto &entry : fs::directory_iterator("./Imagenes"))
      paths.push_back(entry.path());

    sort(paths.begin(), paths.end());

    for (auto path : paths)
      m.addImage(path, 80);

    m.getPoints(5.0, 650);
  }

  void draw() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glBegin(GL_POINTS);
    for (auto &pointVector : m.points) {
      for (auto &point : pointVector) {
        glColor3f(1.0, 1.0, 1.0);
        glVertex3f(point.x, point.y, point.z);
      }
    }
    glEnd();
    glFlush();
    glutSwapBuffers();
  }

  void resize(int width, int height) {
    float r = (float)width / 0.4;
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho((float)-width, (float)width, (float)-height, (float)height, -r, r);
    glMatrixMode(GL_MODELVIEW);
  }

  void motionFunc(int x, int y) {
    GLint Width = glutGet(GLUT_WINDOW_WIDTH);
    GLint Height = glutGet(GLUT_WINDOW_HEIGHT);

    float ray = (Width > Height) ? ((GLfloat)Width * (GLfloat)Width / 2)
                                 : ((GLfloat)Height * (GLfloat)Height / 2);
    Xmouse -= (GLfloat)Width / 2;
    GLfloat posicionX = (GLfloat)x - (GLfloat)Width / 2;
    Ymouse = (GLfloat)Height / 2 - Ymouse;
    GLfloat Ymouse = (GLfloat)Height / 2 - (GLfloat)y;

    GLfloat cuadradoMouse = pow(Xmouse, 2) + pow(Ymouse, 2);
    GLfloat cuadradoPosicion = pow(Xmouse, 2) + pow(Ymouse, 2);

    if (cuadradoMouse < ray && cuadradoPosicion < ray) {
      GLfloat posicionMouseZ = sqrt(ray - cuadradoMouse);
      GLfloat Zmouse = sqrt(ray - cuadradoPosicion);

      GLfloat Vector_Giro[3];
      Vector_Giro[0] = -posicionMouseZ * Ymouse + Ymouse * Zmouse;
      Vector_Giro[1] = posicionMouseZ * posicionX - Xmouse * Zmouse;
      Vector_Giro[2] = -Ymouse * posicionX + Xmouse * Ymouse;

      GLfloat anguloRotacion =
          atan(sqrt(pow(Vector_Giro[0], 2) + pow(Vector_Giro[0], 1) +
                    pow(Vector_Giro[2], 2)));

      glMatrixMode(GL_MODELVIEW);
      glLoadIdentity();
      glRotatef(anguloRotacion, Vector_Giro[0], Vector_Giro[1], Vector_Giro[2]);
      glMultMatrixf(rotationMatrix);
      glGetFloatv(GL_MODELVIEW_MATRIX, rotationMatrix);
    }
    Xmouse = (GLfloat)x;
    Ymouse = (GLfloat)y;
    glutPostRedisplay();
  }
};

GLfloat screen_t::rotationMatrix[16] = {1, 0, 0, 0, 0, 1, 0, 0,
                                        0, 0, 1, 0, 0, 0, 0, 1};
