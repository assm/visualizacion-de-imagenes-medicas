#pragma once
#include "../headers/macros.h"
#include "./vector.h"

struct model {
  vector<CImg<char>> images;
  vector<vector<vec3f>> points;
  int height, width;

  model() {}

  void addImage(string path, int minPointValue) {
    CImg<float> initialImage(path.c_str());
    CImg<char> finalImage = filterPoints(initialImage, minPointValue);

    images.push_back(finalImage);
  }

  void getPoints(float distanceImg, float offset) {
    for (int i = 0; i < images.size(); i++) {
      CImg<char> actualImage = images[i];
      vector<vec3f> imagePoints;
      cimg_forXY(actualImage, x, y) {
        string s1 = to_string(actualImage.atXYZC(x, y, 1, 1));
        string s2 = to_string(actualImage.atXYZC(x + 1, y, 1, 1));
        if (s1[0] != s2[0]) {
          vec3f point((float)x * distanceImg - offset,
                      (float)i * distanceImg - offset, (float)y * distanceImg);
          imagePoints.push_back(point);
        }
      }
      points.push_back(imagePoints);
    }
  }

  void savePoints(string filename) {
    fstream file(filename, ios::out);
    for (auto &pointsVector : points) {
      for (auto &point : pointsVector) {
        file << point.x << ' ' << point.y << ' ' << point.z << endl;
      }
    }
    file.close();
  }

  void readPoints(string filename) {
    fstream file(filename, ios::in);
    vector<vec3f> pointsVector;
    string line;
    while (getline(file, line)) {
      vec3f point;
      sscanf(line.c_str(), "%f %f %f", &point.x, &point.y, &point.z);
      pointsVector.push_back(point);
    }
    points.push_back(pointsVector);
  }

private:
  static CImg<char> filterPoints(CImg<> &image, int minPointValue) {
    CImg<char> newImage(image.width(), image.height());
    for (int i = 0; i < image.width(); i++) {
      for (int j = 0; j < image.height(); j++) {
        int r = image(i, j, 0);
        int g = image(i, j, 1);
        int b = image(i, j, 2);
        newImage(i, j) = (char)(((r + g + b) / 3 > minPointValue) ? 0 : 255);
      }
    }

    return newImage;
  }
};
